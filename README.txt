Microcontent
By Bèr Kessels
sponored by newsphoto.nl
developed for sympal (sympal.nl)


Various times, I heard Dries talking a bout tasks he wants to see done easier. Things he considers "basic stuff to do in Drupal". I stole this list from a mail to the documentation list, but my belowmentioned points, IMO should be done *simple* in Drupal. we can document how one does it. But IMO we should offer code to do this.

* 2. Setting up an about page.*
* 3. Setting up a contact page.*
* 6. Setting a mission statement and slogan.*
* 9. Figuring out the difference between 'story' and 'page' (and
grokking the node system).*

My shot at this, for sympal/drupalCOM is a module called microcontent. I want to take this opportunity to get some input from the community on the workings of this module.

* Every microcontent entry is a node.
* Every microcontent entry is available as a block
* You can choose "special" microcontent "characteristics" for now:
  * site summary (site title, site body)
  * mission (mission title, short mission, long mission)
  * site details (footer)

So the basic site setup is done trough our *content* areas. Blocks are made/moderared trough our famous *content* user interfaces.

We could choose (we did so for sympal/drupalCOM) to prefill the database with dummy missions (edit mission <= link)  and other dummy microcontent (about page etc), but that would mean that the contrib module ships with an install script.

No longer deeply hidden admin pages to change the mission, but living withing the other content.
About pages, and a contact page will be part of my microcontent v2. in that v2 I want to integrate 3rd party modules, such as that contact.module, or other modules that affect small pieces of user-end content on the site.